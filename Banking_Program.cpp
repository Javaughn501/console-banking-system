/*Will be making a banking application
This project will make use of file system in c++ as a database. This project will simulate a bank and the features of a bank.

-Accounts
-Account Types (Savings & Chequings)
-Account Details of a user
-Able to check the user balance

Files:  Users , Account

File: Users-
-FirstName 
-LastName 
-DOB 
-TRN 
-Account#(Auto Generated)

File: Account-
-Account#
-Account Type
-Balance

*/

#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib> 
#include <ctime>
using namespace std;

int menu();
void add_client();
float deposit(int,float);
float withdrawl(int,float);
void account_details(int);
float check_bal(int);

int main()
{
	int decision;
	decision = menu();
	while (decision < 6)
	{
		switch(decision)
		{
			case 1:
				//add client
				cout << "*****Add Client to system*****" << endl;
				add_client();
				cout << "Patient Successfully Added!" << endl;
				break;
			
			case 2:
				//deposit
				int accountnumdep;
				float balancedep,newdep;
				cout << "*****Deposit*****" << endl;
				cout << "Enter Client's Account Number: ";
				cin >> accountnumdep;
				cout << "\n" << "How much are you depositing? ";
				cin >> newdep;
				while(newdep < 0)
				{
					cout << "\n" << "How much are you depositing? ";
					cin >> newdep;
				}
				balancedep = deposit(accountnumdep,newdep);
				cout << balancedep << " New balance is " << balancedep << endl;
				break;
					
			
			case 3:
				//withdraw
				int accountnumwith;
				float balancewith,newwith,checker;
				cout << "*****Withdrawl*****" << endl;
				cout << "Enter Client's Account Number: ";
				cin >> accountnumwith;
				cout << "How much are you withdrawing? ";
				cin >> newwith;
				checker = check_bal(accountnumwith);
				while(checker < newwith)
				{
					cout << "How much are you withdrawing? ";
					cin >> newwith;
				}
				balancewith = withdrawl(accountnumwith,newwith);
				cout << accountnumwith << " Account new balance is " << balancewith << endl;
				break;
				
			case 4:
				//account details
				int accountnumdet;
				cout << "*****Account Details*****" << endl;
				cout << "Enter Client's Account Number: ";
				cin >> accountnumdet;
				account_details(accountnumdet);
				break;
				
			case 5:
				//check balance
				int accountnumbal;
				float balancebal;
				cout << "*****Check Balance*****" << endl;
				cout << "Enter Client's Account Number: ";
				cin >> accountnumbal;
				balancebal = check_bal(accountnumbal);
				cout << "Account " << accountnumbal << " Balance is " << balancebal << endl;
				break;
		}
		decision = menu();
		cout << decision;
	}
	
	return 0;
}

int menu()
{
	int decision;
	cout << "************HERKSHIRE BANK************" << endl;
	cout << "1. Add a client to the system" << endl << "2. Deposit" << endl << "3. Withdraw" << endl << "4. Account Details of a cleint" << endl << "5. Check Balance of an account" << endl << "6. Exit" << endl;
	cout << "Choose an option from the menu above: ";
	cin >> decision;
	
	return decision;
}

void add_client()
{
	/*
	store in file users.
	-FirstName 
	-LastName 
	-DOB 
	-TRN 
	-Account#(Auto Generated)
	
	Store in Account file
	-Account#
	-Account Type
	-Balance
	*/
	// ifstream (read from it). ofstream (write to it).
	int min = 99999;
	string fname,lname,text;
	int trn,accountnum;
	int day,month,year;
	char isdeposit;
	string accounttype;
	int deposit;
	int used_account[10000];
	
	
	ifstream client("Client.txt");
	ofstream temp("temp_client.txt");
	
	ifstream account("Account.txt");
	ofstream temp_account("temp_account.txt");
	
	if (temp.is_open())
	{
		while (getline(client,text))
		{
			temp << text << "\n";
		}
		
		while (getline(account,text))
		{
			temp_account << text << "\n";
		}
		
		cout << "Enter client's first name: ";
		cin >> fname;
		cout << "Enter client's last name: ";
		cin >> lname;
		cout << "Enter client's trn: ";
		cin >> trn;
		cout << "Enter client's DOB" << endl;
		cout << "Day: ";
		cin >> day;
		cout << "Month: ";
		cin >> month;
		cout << "Year: ";
		cin >> year;
		
		cout << "Enter account type (Savings or Chequing): ";
		cin >> accounttype;
		cout << "Are you depositing any money today? (Y or N)";
		cin >> isdeposit;
		isdeposit = toupper(isdeposit);
		
		switch(isdeposit)
		{
			case 'Y':
				cout << "How much are you depositing today: ";
				cin >> deposit;
				break;
			
			case 'N':
				cout << "No deposit today" << endl;
				break;
		}
		
		srand(time(0));
		accountnum = (rand() % 1000000) + min;
		for(int i = 0; i>10000;i++)
		{
			if (accountnum == used_account[i])
			{
				accountnum = (rand() % 1000000) + min;	
			}
		}
		
		temp << fname << "\t" << lname << "\t" << trn << "\t" << day << "\t" << month << "\t" << year << "\t" << accountnum;
		
		temp_account << accountnum << "\t" << accounttype << "\t" << deposit;
		
		client.close();
		temp.close();
		
		account.close();
		temp_account.close();
		
		remove("Client.txt");
		rename("temp_client.txt","Client.txt");
		
		remove ("Account.txt");
		rename("temp_account.txt","Account.txt");
	}
	
	return;  	
}

float deposit(int accountnum,float newdep)
{
	// search the file for the account num , get the balance update it
	// store the new balance ,  print the old file in a new file along with the new update account
	string type;
	int accountn,num;
	float bal,newbal;
	
	ifstream account("Account.txt");
	ofstream temp("temp.txt");
	
	if(account.is_open())
	{
		while (account >> accountn)
		{
			
			account >> type;
			account >> bal;
			
			if (accountn == accountnum)
			{
				newbal = bal+newdep;
				temp << accountn << "\t" << type << "\t" << newbal << "\n";
			}
			if (accountn != accountnum)
				temp << accountn << "\t" << type << "\t" << bal << "\n";
		}
	}
	
	temp.close();
	account.close();
	
	remove("Account.txt");
	rename("temp.txt","Account.txt");
	
	return newbal;
}

float withdrawl(int accountnum,float newwith)
{
	// read contents from the accounts file
	//search for the account number, adjust the balance
	// print everything in a temp file and rename it.
	string type;
	int accountn;
	float bal,newbal;
	
	ifstream account("Account.txt");
	ofstream temp("temp.txt");
	
	if (account.is_open())
	{
		while(account >> accountn)
		{
			account >> type;
			account >> bal;
			
			if(accountn == accountnum)
			{
				newbal = bal-newwith;
				temp << accountnum << "\t" << type << "\t" << newbal << "\n";
			}
			else
				temp << accountnum << "\t" << type << "\t" << bal << "\n";
		}
		
		account.close();
		temp.close();
		
		remove("Account.txt");
		rename("temp.txt","Account.txt");
	}
	
	return newbal;	
}

void account_details(int accountnum)
{
	//read account and client file
	//search account number in both file
	//print client name , account number , account type.
	int accountn,trn,day,month,year,accountnn;
	float bal;
	string type,fname,lname;
	
	ifstream account("Account.txt");
	ifstream client("Client.txt");
	
	if(account.is_open())
	{
		if(client.is_open())
		{
			while (account >> accountn)
			{
				account >> type;
				account >> bal;
				client >> fname;
				client >> lname;
				client >> trn;
				client >> day;
				client >> month;
				client >> year;
				client >> accountnn;
				if (accountn == accountnum)
					cout << "Name: " << fname << " " << lname << endl <<"Account Number: " << accountn << endl << "Account Type: " << type << endl;
			}
		}
	}
	
		
}

float check_bal(int accountnum)
{
	//read account file
	//search for the account number and get the balance.
	int accountn;
	string type;
	float bal,needed_bal;
	
	ifstream account("Account.txt");
	
	if (account.is_open())
	{
		while(account >> accountn)
		{
			account >> type;
			account >> bal;
			if(accountn == accountnum)
				needed_bal = bal;
		}
	}
	return needed_bal;	
}
